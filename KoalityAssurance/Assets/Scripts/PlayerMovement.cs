﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    Quaternion pRot;
    Rigidbody pRB;
    public float moveSpeed;
    public float rotSpeed;
    // Start is called before the first frame update
    void Start()
    {
        pRB = GetComponent<Rigidbody>();

    }

    // Update is called once per frame
    void Update()
    {
        float xAxis = Input.GetAxisRaw("Horizontal");
        float zAxis = Input.GetAxisRaw("Vertical");
        pRot = transform.rotation;
        ///Player Speed
        pRB.velocity = new Vector3((zAxis * Time.deltaTime* moveSpeed), 0, 0);


        ///Player Rotation
        #region
        if (Input.GetAxis("Horizontal") != 0)
        {
            transform.localEulerAngles += new Vector3(pRot.x, rotSpeed * Input.GetAxis("Horizontal"), pRot.z);
        }
        #endregion
    }
}
