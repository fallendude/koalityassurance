﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    //GameObject[] products;
    [SerializeField]
    private List<GameObject> items;
    ConveyorBelt conBelt;

    //Awake & Start
    #region
    void Awake()
    {
        //StoreItemsInScene();
        conBelt = FindObjectOfType<ConveyorBelt>();
    }    
    void Start()
    {
        conBelt.EnqueueNewItems(items); //Enqueueus the list 'Items' to ConveyorBelt script's gameObject 'Items' Queue
        conBelt.SpawnItem();  //Spawns the first item
    }

    #endregion

    //During Gameplay
    void Update()
    {

        if(conBelt.currItem == null)
        {
            conBelt.SpawnItem();
        }
    }

    //Deprecated For now
    #region
    /*
    //Grabs all products in scene and adds them to a list of gameobject 'Items'
    void StoreItemsInScene()
    {
        /*
        products = FindObjectsOfType<Product>();
        if (products != null)
        {
            foreach (Product product in products)
            { items.Add(product.gameObject); }
        }
        else { Debug.Log("No Products found!"); }
        

        //GameObject toyCar = (GameObject)Resources.Load("Prefabs/ToyCar", typeof(GameObject));

        //products = Resources.LoadAll("Prefabs", typeof(GameObject));

        Object[] prefabs = Resources.LoadAll("Prefabs");

        foreach (Object prefab in prefabs)
        {
            if (prefab == null)
            { Debug.LogWarning("No Prefabs here."); }
            items.Add((GameObject)prefab);
        }


            //items.Add(toyCar);
    }
        */
    #endregion

}
