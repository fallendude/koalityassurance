﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    //Grabs the item[x]
    //Needs to turn item rigidbody Off [x]
    //Needs to stay in interactTransform until 

    public Transform interactTransform;
    public float interactRange = 0.5f;
    public LayerMask itemLayers;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButton("Jump"))
        {
            PickUp();
        }
    }

    void PickUp()
    {        
        Collider[] hitItems = Physics.OverlapSphere(interactTransform.position, interactRange, itemLayers);
         
        foreach(Collider item in hitItems)
        {
            Debug.Log("Grabbed the item:" + item.gameObject.name);
            Rigidbody itemRB = item.GetComponent<Rigidbody>();//new
            Destroy(itemRB);//new
            item.gameObject.transform.position = interactTransform.position;
        }

    }

    void OnDrawGizmosSelected()
    {
        if (interactTransform == null)
            return;

        Gizmos.DrawWireSphere(interactTransform.position, interactRange);
    }
}
