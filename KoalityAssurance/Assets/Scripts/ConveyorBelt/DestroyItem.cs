﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyItem : MonoBehaviour
{
    ConveyorBelt m_ConveyorBelt;
    void Start()
    {
        m_ConveyorBelt = FindObjectOfType<ConveyorBelt>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == m_ConveyorBelt.currItem)
        { Destroy(m_ConveyorBelt.currItem); m_ConveyorBelt.currItem = null;}
    }
}
