﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConveyBeltDir : MonoBehaviour
{
    
    public Direction myDirection;
    Rigidbody ItemRB;
    ConveyorBelt m_ConveyorBelt;
    public enum Direction { Up, Down, Left, Right };

    void Awake()
    {
        m_ConveyorBelt = FindObjectOfType<ConveyorBelt>();
    }

    void Update()
    {
        if (ItemRB != null)
        {
            if (myDirection == Direction.Up)
            { ItemRB.velocity = new Vector3(0, 0, m_ConveyorBelt.conveyorBeltSpeed); }
            else if (myDirection == Direction.Down)
            { ItemRB.velocity = new Vector3(0, 0, -m_ConveyorBelt.conveyorBeltSpeed); }
            else if (myDirection == Direction.Left)
            { ItemRB.velocity = new Vector3(-m_ConveyorBelt.conveyorBeltSpeed, 0); }
            else if (myDirection == Direction.Right)
            { ItemRB.velocity = new Vector3(m_ConveyorBelt.conveyorBeltSpeed, 0); }
        }
    }


    void OnTriggerEnter(Collider coll)
    {
        if(coll.gameObject.tag == "Item")
        { ItemRB = coll.gameObject.GetComponent<Rigidbody>(); if (m_ConveyorBelt.currConBelDir != this) { m_ConveyorBelt.currConBelDir = this; } }
    }
    void OnTriggerExit(Collider coll)
    {
        if (coll.gameObject.tag == "Item")
        { ItemRB = null; if (m_ConveyorBelt.currConBelDir == this) { m_ConveyorBelt.currConBelDir = null; } }
    }
    
}
