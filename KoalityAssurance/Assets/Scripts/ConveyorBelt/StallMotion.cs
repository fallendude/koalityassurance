﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StallMotion : MonoBehaviour
{
    //bring item to center                   [x]
    //have item sit for a second and rotate  [x]
    //then send it in a direction            [x]

    public GameObject item;
    public Transform center;
    bool itemInThePlace;

    public Rotation myRotation;
    public enum Rotation { Forward, Backward, Leftward, Rightward};   
    Vector3 myDirection;

    bool timeToGo;
    [SerializeField]
    float waitTime;
    int x, z;

    void SetMyDirection()
    {
        x = 0;
        z = 0;
        if (myRotation == Rotation.Forward)
        { myDirection = new Vector3(0, -90, 0); z = 1; }
        else if (myRotation == Rotation.Backward)
        { myDirection = new Vector3(0, 90, 0); z = -1; }
        if (myRotation == Rotation.Leftward)
        { myDirection = new Vector3(0, 180, 0); x = -1; }
        if (myRotation == Rotation.Rightward)
        { myDirection = new Vector3(0, 90, 0); x = 1; }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Item")
        {
            item = other.gameObject;
            itemInThePlace = true;
            item.transform.position = new Vector3(center.position.x, item.transform.position.y, center.position.z);
            SetMyDirection();
            item.transform.eulerAngles = myDirection;
            StartCoroutine(WaitSomeTime(waitTime));
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Item")
        {
            item = null;
            itemInThePlace = false;
            timeToGo = false;
        }
    }

    IEnumerator WaitSomeTime(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        timeToGo = true;
    }

    void Update()
    {
        if (timeToGo && item != null)
        { item.transform.position = Vector3.MoveTowards(item.transform.position, (item.transform.position + new Vector3(x,0,z)), 1f); }
    }

}
