﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConveyorBelt : MonoBehaviour
{
    Queue<GameObject> Items;    
    public Transform itemSpawn;
    public GameObject currItem;
    GameObject nextItem;
      
    /*
    [SerializeField]
    private List<Transform> conveyorBeltPoints;
    public float conveyorBeltSpeed;
    int currPoint = 0;
    float pointRange = 1f;
    /// <summary>
    /// Not Using for now.
    /// </summary>
    */


    //Child Script Variables
    #region
    ConveyBeltDir[] conBelDirs;
    public ConveyBeltDir currConBelDir;
    public float conveyorBeltSpeed;
    #endregion
    


    //Awake
    #region
    void Awake()
    {
        Items = new Queue<GameObject>();
        conBelDirs = FindObjectsOfType<ConveyBeltDir>();
        //GetConveyorBeltPointTrans(GameObject.FindGameObjectsWithTag("Point"));
    }
    #endregion
    /*
    void GetConveyorBeltPointTrans(GameObject[] goPointArray)
    {        
        for(int i = 0; i < goPointArray.Length; i++)
        {
            conveyorBeltPoints.Add(goPointArray[i].transform);
        }        
    }

    
    void Update()
    {
        if(currItem != null && conveyorBeltPoints != null)//&& player.heldItem != currItem)
        {
            Debug.Log(conveyorBeltPoints[currPoint].localPosition);
            //currItem.transform.Translate((conveyorBeltPoints[currPoint].position - currItem.transform.position).normalized * Time.deltaTime * conveyorBeltSpeed);

            currItem.transform.Translate(new Vector3 ((conveyorBeltPoints[currPoint].position.x - currItem.transform.position.x), currItem.transform.position.y, (conveyorBeltPoints[currPoint].position.z - currItem.transform.position.z)) * Time.deltaTime * conveyorBeltSpeed);

            //Did we reach point? if so, next point
            if (Mathf.Abs(conveyorBeltPoints[currPoint].position.x - currItem.transform.position.x) < pointRange && Mathf.Abs(conveyorBeltPoints[currPoint].position.z - currItem.transform.position.z) < pointRange) // if the camera reaches the next target and there are still targets left iterate target step
            {
                Debug.Log("Made it to:" + conveyorBeltPoints[currPoint].gameObject.name);
                currItem.transform.position = conveyorBeltPoints[currPoint].position;
                currPoint++;
            }


        }
    }
    */

    //Manage Item Queue
    #region
    public void SpawnItem()
    {
        if (Items.Count != 0)
        {
            nextItem = Items.Dequeue();
            Vector3 newItemPos = new Vector3(itemSpawn.transform.position.x, itemSpawn.transform.position.y, itemSpawn.transform.position.z);
            Quaternion rotation = itemSpawn.transform.rotation;
            currItem = Instantiate(nextItem, newItemPos, rotation);
        }
    }

    public void ClearItems()
    {
        Items.Clear();
    }

    public void EnqueueNewItems(List<GameObject> newItems)
    {
        foreach (GameObject item in newItems)
        { Items.Enqueue(item); }
    }
    #endregion
}
