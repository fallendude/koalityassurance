﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Product", menuName = "Products/Item")]
public class Item : ScriptableObject
{
    [SerializeField] string Name;
    [SerializeField] GameObject Needs;
}
